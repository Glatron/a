# Installation

$ sh install_opencv_contrib.sh

# In alg_comparison or multi_obj:

$ sh compile_cmake.sh

$ ./executable [\<id\>]

# id
0: BOOSTING 

1: MIL

2: KCF

3: TLD

4: MEDIANFLOW

5: GOTURN

6: MOSSE

7: CSRT

# Sub Modules

MOTLD: multi object implementation of tld, reuses calculations to make the tracking more efficient - as long as all objects have the same aspect ratio

GOTURN: another algorithm from caffe, can be implemented in opencv