#include "detection.hpp"

std::vector<cv::Rect2d> object_shape_detection (cv::Mat &im) //the image is the input
{
    // Setup SimpleBlobDetector parameters.
    cv::SimpleBlobDetector::Params params;

    // Change thresholds
    params.filterByColor = false;
    //params.blobColor = 0;
    //params.minThreshold = 0;
    //params.maxThreshold = 255; //higher threshold makes it so we can find lighter objects


    params.filterByConvexity = true;
    params.filterByInertia = true;
    params.filterByArea = true;
    params.filterByCircularity = true;

    params.minArea = 500;
    params.minCircularity = static_cast<float>(0.2);
    params.minConvexity = static_cast<float>(0.2);
    params.minInertiaRatio = static_cast<float>(0.2);

    // Storage for blobs
    std::vector<cv::KeyPoint> keypoints;

    // Set up detector with params
    cv::Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);

    // Detect blobs
    detector->detect( im, keypoints);



    unsigned long number_of_markers = keypoints.size();

    std::vector<cv::Rect2d> markers; //used to pass to next step
    cv::Rect2d aux;

    for(uint i=0; i<number_of_markers;i++)
    {
        int radius = keypoints[i].size*2/3;
        aux.x=keypoints[i].pt.x - radius; //go left
        aux.y=keypoints[i].pt.y - radius; //go up
        aux.width = radius*2;
        aux.height = radius*2;
        markers.push_back(aux);
    }

    // Show blobs:
    // Draw detected blobs as red circles.
    // DrawMatchesFlags::DRAW_RICH_KEYPOINTS flag ensures
    // the size of the circle corresponds to the size of blob

    drawKeypoints( im, keypoints, im, cv::Scalar(0,0,255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );

    return markers;
}


cv::Mat object_color_detection (cv::Mat im) //the image is the input
{

    cv::Mat HSV_image, GRAY_image;

    cvtColor(im,HSV_image,cv::COLOR_BGR2HSV);

    medianBlur(HSV_image, HSV_image, 5); //soften

    cvtColor(HSV_image, GRAY_image,cv::COLOR_BGR2GRAY);

    return GRAY_image;
}


/*
int mainTest()
{
    
    std::string imgPath1 = "/home/user/Desktop/real1.png"; //relevant for static image detection only

    cv::Mat im = readImage(imgPath1);
    cv::Mat res1;

    res1 = object_color_detection(im);
    std::vector<cv::Rect2d> nextStageVar = object_shape_detection(res1);
    
   return 0;
}
*/